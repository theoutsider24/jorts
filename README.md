# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is an open-source Java-based real-time strategy game engine for use as a testbed for AI research. 
* The FYP branch is maintained for historical reasons to store the version of the software submitted as my Final Year Project
* Currently the master branch is where new work will go, additional branches will be added should there be interest in contribution or additional extensive development

### How do I get set up? ###
This repo contains one eclipse project containing two seperate runnable applications, JORTS and HARTS
Both contain a Main class which contains the main method
JORTS must be supplied with the name of a game definition file as a command line argument (game1 is the default supplied file)


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

I can be contacted at mattcoyle48@gmail.com for any queries regarding this software
All code is written by me (Except for one method for Bresenham line drawing)
All art assets are placeholders which have been pulled from all over the internet including many from Age of Empires 2 (I am not an artist)
package JORTS.behaviour.orders;

import org.jsfml.system.Vector2f;

import JORTS.behaviour.flowField.Field;
import JORTS.common.CommonFunctions;
import JORTS.core.Main;
import JORTS.gameElements.units.Entity;

public class FleeOrder extends Order {
	Entity attacker;
	public FleeOrder(Entity e)
	{
		super();
		attacker=e;
	}
	@Override
	public Vector2f getVector(Entity e) {
		int safetyDistanceSqr=(300*300);
		if(CommonFunctions.getDistSqr(e.getPosition(), attacker.getPosition())+(attacker.attackRange*attacker.attackRange)<safetyDistanceSqr)
			return new Vector2f(e.getPosition().x-attacker.getPosition().x,e.getPosition().y-attacker.getPosition().y);
		else
		{
			Order.IdleOrder.issue(e);
			return new Vector2f(0,0);
		}
	}
}

package JORTS.behaviour.orders;

import static JORTS.common.Constants.CELL_SIZE;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.jsfml.system.Vector2f;

import JORTS.behaviour.flowField.Field;
import JORTS.behaviour.movement.MovementPattern;
import JORTS.behaviour.movement.Square;
import JORTS.common.CommonFunctions;
import JORTS.core.Main;
import JORTS.gameElements.units.Entity;

public class MoveOrder extends Order {
	MovementPattern pattern;
	public MoveOrder()
	{
		super();
		flowField = new Field(Main.worldMap);
	}
	public void init(Vector2f loc,int size,float minX,float maxX, float minY,float maxY)
	{
		targetLocation=loc;
		t = new Thread(new Runnable(){
			@Override
			public void run() {
				flowField.openCellatPos(loc,size,minX,maxX,minY,maxY);			
				initialised=true;
			}
		},"Move_Order_Thread");
		t.start();	
	}
	
	public void init(Vector2f loc, ArrayList<Entity> entities)
	{		
		pattern = new Square(loc, entities,this);
		float minX,maxX,minY,maxY;
		minX=loc.x;
		maxX=loc.x;
		
		minY=loc.y;
		maxY=loc.y;
		
		for(Entity e:entities)
		{
			if(e.getPosition().x>maxX)
				maxX=e.getPosition().x;
			if(e.getPosition().x<minX)
				minX=e.getPosition().x;
			
			if(e.getPosition().y>maxY)
				maxY=e.getPosition().y;
			if(e.getPosition().y<minY)
				minY=e.getPosition().y;
		}
		
		double requiredArea=0;
		for(Entity e:entities)
			requiredArea+=((e.getRadius()+e.influenceRange)*(e.getRadius()+e.influenceRange)*2);
		int requiredCells=(int) (requiredArea/(CELL_SIZE*CELL_SIZE));
		
		
		init(loc,0,minX,maxX,minY,maxY);
		//init(loc,requiredCells,minX,maxX,minY,maxY);
	}
	@Override
	public Vector2f getVector(Entity e)
	{
		Vector2f v = super.getVector(e);	
		v = Vector2f.add(pattern.getVector(e), v);

	//	if(pattern.hasArrived(e))
	//		return Vector2f.ZERO;
		v = CommonFunctions.normaliseVector(v);
		if(v.equals(Vector2f.ZERO)&&flowField.calculated)
		{
			//Order.IdleOrder.issue(e);
			return new Vector2f(0,0);
		}
		return v;
	}
}

package JORTS.behaviour.movement;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

import org.jsfml.graphics.Color;
import org.jsfml.system.Vector2f;

import JORTS.behaviour.orders.Order;
import JORTS.common.CommonFunctions;
import JORTS.gameElements.units.Entity;

public abstract class MovementPattern
{
	ArrayList<Entity> units;
	HashMap<Entity, Boolean> arrived;
	Vector2f generalDirection;
	int dimension;
	Vector2f avgPos;
	Vector2f target;
	int spacingMul = 4;
	Order order;
	
	protected MovementPattern(){}
	protected MovementPattern(Vector2f loc, ArrayList<Entity> entities, Order o)
	{
		order = o;
		units = new ArrayList<Entity>();
		arrived = new HashMap<Entity, Boolean>();
		target = loc;
		units.addAll(entities);		
		for(Entity e:units)
			arrived.put(e, false);
		
		setDimension();
				
		int n = units.size();		
		avgPos=new Vector2f(0,0);
		for(Entity e:units)
		{
			e.setOutlineColor(e.player.color);
			avgPos = Vector2f.add(avgPos, e.getPosition());	
		}
		avgPos = Vector2f.div(avgPos, n);
		generalDirection = CommonFunctions.normaliseVector(Vector2f.sub(target, avgPos));	
		
	}
	public abstract void setDimension();
	public abstract Vector2f getVector(Entity e);
	public boolean hasArrived(Entity e)
	{
		return arrived.get(e);
	}
}

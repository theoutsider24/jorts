package JORTS.behaviour.movement;

import java.util.ArrayList;
import java.util.Comparator;
import static JORTS.common.CommonFunctions.*;

import org.jsfml.graphics.Color;
import org.jsfml.system.Vector2f;

import JORTS.behaviour.flowField.Field;
import JORTS.behaviour.orders.Order;
import JORTS.common.CommonFunctions;
import JORTS.gameElements.units.Entity;

public class Square extends MovementPattern
{
	@Override
	public void setDimension()
	{
		dimension = (int) Math.ceil(Math.sqrt(units.size()));
	}
	public Square(Vector2f loc, ArrayList<Entity> entities, Order o)
	{
		super(loc,entities,o);		
		sortUnits();
	}
	public void sortUnits()
	{
		units.sort(new Comparator<Entity>() {
			@Override
			public int compare(Entity e1, Entity e2)
			{
				return (int) (distToLine(e1.getPosition(),generalDirection,target) - distToLine(e2.getPosition(),generalDirection,target));
			}
		});
		
		Vector2f perpVect = CommonFunctions.rotateVectorDeg(generalDirection, Vector2f.ZERO, 90);
		double slope = CommonFunctions.getSlope(perpVect);
		
		for(int i=0;i<dimension;i++) 
		{
			if(dimension*i<=units.size())
			{
				units.subList(dimension*i,units.size()>=(dimension*i)+dimension? (dimension*i)+dimension : units.size()).sort(new Comparator<Entity>() {
					@Override
					public int compare(Entity e1, Entity e2)
					{
						Vector2f v1 = Vector2f.sub(e1.getPosition(), e2.getPosition());
						
						float dotProd = (v1.x*perpVect.x)+(v1.y*perpVect.y);
						return (int) dotProd;
					}
				});
			}
		}
	}
	@Override
	public Vector2f getVector(Entity e)
	{
		Vector2f v = Vector2f.ZERO,v1 = Vector2f.ZERO,v2 = Vector2f.ZERO;

		Vector2f space = Vector2f.mul(generalDirection,e.getRadius()*-spacingMul);
		if(units!=null&&units.contains(e))
		{			
			//check if source unit has arrived
			if(units.indexOf(e)/dimension==0 && units.indexOf(e)%dimension==0)
			{
				if(order.flowField.getCellAtPos(e.getPosition()).isGoal)//getDist(e.getPosition(),target)<20)
				{
					arrived.put(e,true);
				}
				return v;
			}

			if(units.indexOf(e)%dimension>0&&canSeeUnit(e,units.indexOf(e)-1))
			{
				v1= getV(e,units.indexOf(e)-1,new Vector2f(space.y,-space.x),5);
			}
			if(units.indexOf(e)/dimension>0&&canSeeUnit(e,units.indexOf(e)-dimension))
			{
				v2=getV(e,units.indexOf(e)-dimension,new Vector2f(space.x,space.y),5);
			}
		}
		int arrivedCheck = 0;
		if(units.indexOf(e)%dimension==0)
			arrivedCheck++;
		else if(hasUnitArrived(units.indexOf(e)-1)&&canSeeUnit(e,units.indexOf(e)-1))
		{
			 if(onPoint(e,units.indexOf(e)-1,new Vector2f(space.y,-space.x)))
			 {
				arrivedCheck++;
				v1=Vector2f.mul(v1,5);
			 }
			 else
			 {
				 v1=Vector2f.mul(v1,.3f);
			 }
		}
		if(units.indexOf(e)/dimension==0)
			arrivedCheck++;
		else if(hasUnitArrived(units.indexOf(e)-dimension)&&canSeeUnit(e,units.indexOf(e)-dimension))
		{
			if(onPoint(e,units.indexOf(e)-dimension,new Vector2f(space.x,space.y)))
			{
				arrivedCheck++;
				v2=Vector2f.mul(v2,5);
			}
			else
			{
				v2=Vector2f.mul(v2,.3f);
			}
		}
			
		arrived.put(e, arrivedCheck==2);	
		
		if(arrived.get(e))
			return Vector2f.neg(order.flowField.getFlowAtPos(e.getPosition()));
			//return Vector2f.ZERO;
			
		v = Vector2f.add(v1, v2);
		return v;
	}
	public boolean onPoint(Entity e, int i, Vector2f dist)
	{
		try
		{
			Entity target = units.get(i);
			Vector2f v = Vector2f.add(target.getPosition(),dist);
			v = Vector2f.sub(v, e.getPosition());
			return getLength(v)<=1;//getLength(dist);
		}catch(IndexOutOfBoundsException ex){return true;}
		
	}
	public boolean canSeeUnit(Entity e,int i)
	{
		try{
			Field f = order.flowField;
			return f.hasLineOfSight(f.getCellAtPos(units.get(i).getPosition()), f.getCellAtPos(e.getPosition()));
		}catch(IndexOutOfBoundsException ex){return true;}
	}
	public boolean hasUnitArrived(int i)
	{
		try{
			return arrived.get(units.get(i));
		}catch(IndexOutOfBoundsException ex){return true;}		 
	}
	public Vector2f getV(Entity e,int targetIndex, Vector2f space, float multiplier)
	{
		Vector2f v = Vector2f.ZERO;
		try
		{
			Entity target = units.get(targetIndex);
			if(target.currentOrder!=this.order)
				throw new IndexOutOfBoundsException();
			v = space;
			v = Vector2f.add(target.getPosition(),v);
			if(!order.flowField.getCellAtPos(v).isOpen())
				return Vector2f.ZERO;
			v = Vector2f.sub(v, e.getPosition());
			if(getLength(v)>1)v= normaliseVector(v);
			v = Vector2f.mul(v, multiplier);
		}catch(IndexOutOfBoundsException ex){}
		return v;
	}
}

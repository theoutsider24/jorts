package JORTS.gameElements;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.SingleSelectionModel;

import org.jsfml.graphics.Drawable;
import org.jsfml.graphics.RenderStates;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.system.Vector2f;
import org.jsfml.system.Vector2i;

public class AnimationManager implements Drawable 
{
	private static AnimationManager man;
	private static ArrayList<Animation> animations = new ArrayList<Animation>(); 
	private static HashMap<String,Animation> standardImages = new HashMap<String,Animation>();
	private AnimationManager()
	{
	}
	private static void init()
	{
		Vector2f size = new Vector2f(6f,8.5f);
		Animation a=new Animation("targetPoint.png",new Vector2i(1,1),0,new Vector2i(9,13));
		a.setOrigin(Vector2f.div(size, 2));
		a.setSize(size);
		standardImages.put("targetPoint",a);
	}
	@Override
	public void draw(RenderTarget arg0, RenderStates arg1) {
		for(Animation a:animations)
			arg0.draw(a);
	}
	public static AnimationManager getInstance()
	{
		if(man==null)
		{
			man = new AnimationManager();
			init();
		}
		return man;
	}
	public static void addAnimation(Animation a)
	{
		animations.add(a);
	}
	public static void removeAnimation(Animation a)
	{
		animations.remove(a);
	}
	public static Animation getStandardAnimation(String s)
	{
		return standardImages.get(s);
	}
}
